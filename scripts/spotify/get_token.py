import argparse
import json
import spotipy
from spotipy.oauth2 import SpotifyOAuth



def main():
    parser = argparse.ArgumentParser(description='Get temporary Spotify API token using Code authorization.')
    parser.add_argument('--client-id', metavar='client_id', type=str,
                        help='Spotify client ID.', required=True)
    parser.add_argument('--client-secret', metavar='client_secret', type=str,
                        help='Spotify client secret.', required=True)
    parser.add_argument('--redirect-uri', metavar='redirect_uri', type=str,
                        help='Spotify client redirect URI ', required=True)
    parser.add_argument('--output-dir', metavar='output_dir', type=str,
                        help='Custom output path. (Optional)')
    args = parser.parse_args()

    scope='user-read-private user-read-playback-state user-modify-playback-state'
    sp=spotipy.Spotify(auth_manager=SpotifyOAuth(
        scope=scope,
        client_id=args.client_id,
        client_secret=args.client_secret,
        redirect_uri=args.redirect_uri))
    
    # Possible optimization using cached token (.get_cached_token()) For now, get_access_token() will suffice.
    token=sp.auth_manager.get_access_token()

    filepath = args.output_dir + '/token.json'

    token_json = json.dumps(token)
    token_json_formatted = token_json.replace(',', ',\n')

    with open(filepath,'w') as f:
        f.write(token_json_formatted)

    print("Token printed to: " + filepath)

if __name__ == '__main__':
    main()