function(target_install_library)
add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD        # Adds a post-build event to MyTest
COMMAND ${CMAKE_COMMAND} -E copy_if_different  # which executes "cmake - E copy_if_different..."
    "${PROJECT_SOURCE_DIR}/libs/test.dll"      # <--this is in-file
    $<TARGET_FILE_DIR:MyTest>)                 # <--this is out-file path 
endfunction()