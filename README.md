# Dionysus

## Design
* Observer Pattern

## Setup
### Spotify
- Expects Spotify API token at _data/token.txt_
- Above token supplied by _scripts/spotify/get_token.py_.

## TODO
- Actually fill README
- Logging (probably boost).
- Testing (probablt GTest).
- Split main.cpp into app file.
- Properly install spotify-json custom port.
- Setup pipeline.
- Split Spotify into plugin.
- Possibly add Sonos Kernel.

## Personal context

### Experience gained
- Observer Pattern.
- Vcpkg manifest install.
- CMake variables to C++ source code.
- OAth 2.0.
- Communication between languages via serialization.
