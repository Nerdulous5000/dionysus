#include <utils/KernelTest.h>
#include <utils/SubjectTest.h>
#include <gtest/gtest.h>

TEST(KernelTests, GetSubject)
{
    dionysus::test::KernelTest kernel{};
    constexpr auto idTest{"subjectTest"};

    auto subjectTest{std::make_shared<dionysus::test::SubjectTest>()};
    subjectTest->setId(idTest);

    kernel.addSubject(subjectTest);

    const auto found{kernel.getSubject(idTest)};


    EXPECT_NE(found, nullptr);
}