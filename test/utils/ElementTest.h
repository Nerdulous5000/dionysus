#pragma once

#include <dionysus/Element.h>
#include <array>
#include <vector>
#include <string>

namespace dionysus
{
    namespace test
    {
        class ElementTest : public Element
        {
        public:
            ElementTest() = default;


            void setPropertyVector(const std::vector<int>& value);
            [[nodiscard]] const std::vector<int>& getPropertyVector() const;
            
            void setPropertyArray(const std::array<int, 3>& value);
            [[nodiscard]] const std::array<int, 3>& getPropertyArray() const;
            
            void setPropertyString(std::string_view value);
            [[nodiscard]] std::string_view getPropertyString() const;
            
            void setPropertyDouble(double value);
            [[nodiscard]] double getPropertyDouble() const;
            
            void setPropertyFloat(float value);
            [[nodiscard]] float getPropertyFloat() const;
            
            void setPropertyUnsignedInt(int value);
            [[nodiscard]] unsigned int getPropertyUnsignedInt() const;
            
            void setPropertyInt(int value);
            [[nodiscard]] int getPropertyInt() const;
            
            void setPropertyBool(bool value);
            [[nodiscard]] bool getPropertyBool() const;


        protected:
            void onDraw(const dionysus::Window& value) override;
            void deserialize(const dionysus::Serialization& value);

        private:
            std::vector<int> propertyVector{};
            std::array<int, 3> propertyArray{};
            std::string propertyString{};
            double propertyDouble{};
            float propertyFloat{};
            unsigned int propertyUnsignedInt{};
            int propertyInt{};
            bool propertyBool{};
        };
    }
}
