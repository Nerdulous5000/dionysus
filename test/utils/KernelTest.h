#include <dionysus/Kernel.h>

namespace dionysus
{
    namespace test
    {
        class KernelTest : public Kernel
        {
        public:
            KernelTest() = default;

            int onInit() override;
            int onUpdate() override;

        private:
        };
    }
}