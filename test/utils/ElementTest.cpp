#include <utils/ElementTest.h>

void dionysus::test::ElementTest::setPropertyVector(const std::vector<int>& value)
{
    this->propertyVector = value;
}

const std::vector<int>& dionysus::test::ElementTest::getPropertyVector() const
{
    return this->propertyVector;
}

void dionysus::test::ElementTest::setPropertyArray(const std::array<int, 3>& value)
{
    this->propertyArray = value;
}

const std::array<int, 3>& dionysus::test::ElementTest::getPropertyArray() const
{
    return this->propertyArray;
}

void dionysus::test::ElementTest::setPropertyString(std::string_view value)
{
    this->propertyString = value;
}

std::string_view dionysus::test::ElementTest::getPropertyString() const
{
    return this->propertyString;
}

void dionysus::test::ElementTest::setPropertyDouble(double value)
{
    this->propertyDouble = value;
}

double dionysus::test::ElementTest::getPropertyDouble() const
{
    return this->propertyDouble;
}

void dionysus::test::ElementTest::setPropertyFloat(float value)
{
    this->propertyFloat = value;
}

float dionysus::test::ElementTest::getPropertyFloat() const
{
    return this->propertyFloat;
}

void dionysus::test::ElementTest::setPropertyUnsignedInt(int value)
{
    this->propertyUnsignedInt = value;
}

unsigned int dionysus::test::ElementTest::getPropertyUnsignedInt() const
{
    return this->propertyUnsignedInt;
}

void dionysus::test::ElementTest::setPropertyInt(int value)
{
    this->propertyInt = value;
}

int dionysus::test::ElementTest::getPropertyInt() const
{
    return this->propertyInt;
}

void dionysus::test::ElementTest::setPropertyBool(bool value)
{
    this->propertyBool = value;
}

bool dionysus::test::ElementTest::getPropertyBool() const
{
    return this->propertyBool;
}

void dionysus::test::ElementTest::onDraw(const dionysus::Window&)
{
    // Currently nothing is drawn as graphics are not tested.
}

void dionysus::test::ElementTest::deserialize(const dionysus::Serialization& value)
{
    this->propertyVector = value.get<std::vector<int>>("propertyVector");
    this->propertyArray = value.get<std::array<int, 3>>("propertyArray");
    this->propertyString = value.get<std::string>("propertyString");
    this->propertyDouble = value.get<double>("propertyDouble");
    this->propertyFloat = value.get<float>("propertyFloat");
    this->propertyUnsignedInt = value.get<unsigned int>("propertyUnsignedInt");
    this->propertyInt = value.get<int>("propertyInt");
    this->propertyBool = value.get<bool>("propertyBool");
}
