#include <utils/ObserverTest.h>

using namespace dionysus::test;

void ObserverTest::update(const Event*)
{
    this->updated = true;
}

bool ObserverTest::getUpdated() const
{
    return this->updated;
}
