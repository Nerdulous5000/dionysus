#include <dionysus/Observer.h>

namespace dionysus
{
    namespace test
    {
        /// <summary>
        /// The point of this class is to create a flag to mark that an observer can successfully be notified when an observer changes
        /// state.
        /// </summary>
        class ObserverTest : public dionysus::Observer
        {
        public:
            ObserverTest() = default;

            void update(const  Event* value) override;
            bool getUpdated() const;

        private:
            bool updated{false};
        };
    }
}