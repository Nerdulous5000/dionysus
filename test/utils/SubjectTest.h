#include <dionysus/Subject.h>

namespace dionysus
{
    namespace test
    {
        class SubjectTest : public Subject
        {
        public:
            SubjectTest() = default;

            /// <summary>
            /// Used to update state of the subject, expecting observers to be notified.
            /// </summary>
            void trigger() const;
        };
    }
}