#include <dionysus/Serialization.h>
#include <dionysus/FactoryElement.h>
#include <gtest/gtest.h>
#include <utils/ElementTest.h>

TEST(FactoryElement, FactoryRegister)
{
    dionysus::FactoryElement factory;
    factory.registerElement<dionysus::test::ElementTest>();

    // Currently verifying element was added by checking size of registered elements.
    // This will be done until the registered element type naming convention is decided upon.
    const auto elements{factory.getRegisteredElements()};

    EXPECT_EQ(std::size(elements), 1);
}

TEST(FactoryElement, CreateType)
{
    dionysus::FactoryElement factory;
    factory.registerElement<dionysus::test::ElementTest>();

    constexpr auto pathConfig{"test/ElementConfig/ElementTestConfig.config"};
    const auto config{dionysus::Serialization::LoadFromFile(pathConfig)};
    const auto element = factory.createFromConfig(config);

    const auto elementTest{dynamic_cast<dionysus::test::ElementTest*>(element.get())};

    EXPECT_NE(elementTest, nullptr);
}