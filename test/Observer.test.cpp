#include <utils/ObserverTest.h>
#include <utils/SubjectTest.h>
#include <gtest/gtest.h>


TEST(Observer, ObserverUpdate)
{
    auto observer{std::make_shared<dionysus::test::ObserverTest>()};
    auto subject{std::make_shared<dionysus::test::SubjectTest>()};

	subject->attach(observer);
    
    EXPECT_FALSE(observer->getUpdated());

    subject->trigger();

    EXPECT_TRUE(observer->getUpdated());
}