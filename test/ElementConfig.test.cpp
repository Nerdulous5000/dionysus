#include <dionysus/Serialization.h>
#include <utils/ElementTest.h>
#include <gtest/gtest.h>

TEST(ElementConfig, ElementTestConfig)
{
    // constexpr auto pathConfig{"test/ElementConfig/ElementTestConfig.config"};
    // const auto config{dionysus::Serialization::LoadFromFile(pathConfig)};
    // 
    // const auto element{std::make_unique<dionysus::test::ElementTest>()};
    // 
    // element->deserialize(config);
    // 
    // const auto elementTest{dynamic_cast<dionysus::test::ElementTest*>(element.get())};
    // 
    // const std::vector<int> propertyVectorExpected{1, 2, 3};
    // constexpr std::array<int, 3> propertyArrayExpected{4, 5, 6};
    // constexpr auto propertyStringExpected{"test"};
    // constexpr auto propertyDoubleExpected{7.0};
    // constexpr auto propertyFloatExpected{8.0f};
    // constexpr unsigned int propertyUnsignedIntExpected{9};
    // constexpr int propertyIntExpected{10};
    // constexpr auto propertyBoolExpected{true};
    // 
    // 
    // ASSERT_EQ(elementTest->getPropertyVector(), propertyVectorExpected);
    // ASSERT_EQ(elementTest->getPropertyArray(), propertyArrayExpected);
    // ASSERT_EQ(elementTest->getPropertyString(), propertyStringExpected);
    // ASSERT_EQ(elementTest->getPropertyDouble(), propertyDoubleExpected);
    // ASSERT_EQ(elementTest->getPropertyFloat(), propertyFloatExpected);
    // ASSERT_EQ(elementTest->getPropertyUnsignedInt(), propertyUnsignedIntExpected);
    // ASSERT_EQ(elementTest->getPropertyInt(), propertyIntExpected);
    // ASSERT_EQ(elementTest->getPropertyBool(), propertyBoolExpected);

}
