#include <dionysus/FactoryElement.h>
#include <dionysus/spotify/KernelSpotify.h>

int main()
{
    dionysus::spotify::KernelSpotify kernel;
    kernel.setTimeStep(std::chrono::duration<double, std::milli>{1000});

    return kernel.run();
}
