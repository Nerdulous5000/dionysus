#pragma once

#include <filesystem>
#include <nlohmann/json.hpp>
#include <optional>
#include <string>

namespace dionysus
{
    /// <summary>
    /// Given a file path, append it to the path of the data directory.
    /// </summary>
    /// <param name="value"></param>
    /// <returns> Returns the filepath if the file exists, otherwise returns
    /// std::nullopt.</returns>
    std::optional<std::filesystem::path> getPathData(const std::filesystem::path& value);

    /// <summary>
    /// Given a file path, append it to the path of the project.
    /// </summary>
    /// <param name="value"></param>
    /// <returns> Returns the filepath if the file exists, otherwise returns
    /// std::nullopt.</returns>
    std::optional<std::filesystem::path> getPathProject(std::string_view value);

    /// <summary>
    /// Given a path to a file in the data directory, serialize it to a JSON object.
    /// </summary>
    /// <param name="value">Path to json file.</param>
    /// <returns>Deserialized file.</returns>
    // nlohmann::json LIB_DIONYSUS_EXPORT parseJsonFile(const std::filesystem::path& value);
    // 
    // nlohmann::json parseJsonString(std::string_view value);

}