#pragma once
#include <string>

namespace dionysus
{

    class Song
    {
    public:
        Song();

        void setName(std::string_view value);
        std::string_view getName() const;

        void setArtist(std::string_view value);
        std::string_view getArtist() const;
        
        void setAlbumName(std::string_view value);
        std::string_view getAlbumName() const;
        
        void setAlbumUrl(std::string_view value);
        std::string_view getAlbumUrl() const;

    private:
        std::string name{};
        std::string artist{};
        std::string albumName{};
        std::string albumUrl{};
    };
}