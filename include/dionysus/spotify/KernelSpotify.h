#pragma once

#include <dionysus/Kernel.h>
#include <dionysus/Song.h>

#ifdef WIN32
#pragma warning(push, 0)
#endif

#include <restc-cpp/RequestBuilder.h>
#include <restc-cpp/restc-cpp.h>

#ifdef WIN32
#pragma warning(pop)
#endif

namespace dionysus
{
    namespace spotify
    {
        class KernelSpotify : public Kernel
        {
        public:
            KernelSpotify() = default;

        protected:
            int onInit() override;
            int onUpdate() override;

        private:
            std::string getToken();

            std::unique_ptr<restc_cpp::RestClient> client{restc_cpp::RestClient::Create()};
            std::string songIdPrevious{};
        };
    }
}