#pragma once

#include <dionysus/Serialization.h>
#include <optional>

#ifdef WIN32
#pragma warning(push, 0)
#endif

#include <restc-cpp/RequestBuilder.h>
#include <restc-cpp/restc-cpp.h>

#ifdef WIN32
#pragma warning(pop)
#endif

namespace dionysus
{
    namespace spotify
    {
        enum class RequestType
        {
            None,
            Get,
            Post,
        };

        class Request
        {
        public:
            Request() = default;
            void send(restc_cpp::RestClient* value);

            [[nodiscard]] const std::optional<dionysus::Serialization>& getBody() const;

            void setEndpoint(std::string_view value);
            [[nodiscard]] std::string_view getEndpoint() const;

            void setToken(std::string_view value);
            [[nodiscard]] std::string_view getToken() const;

            void setType(dionysus::spotify::RequestType value);
            [[nodiscard]] dionysus::spotify::RequestType getType() const;

        private:
            std::optional<dionysus::Serialization> body;
            std::string endpoint{};
            std::string token{};
            dionysus::spotify::RequestType type{};
        };
    }
}