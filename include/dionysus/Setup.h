#pragma once

namespace dionysus
{
    // Forward declaration to avoid cyclic dependency with dionysus/Factory.h
    class Kernel;

    void Setup(dionysus::Kernel& value);

}