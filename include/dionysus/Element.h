#pragma once

#include <array>
#include <dionysus/Serialization.h>
#include <dionysus/Window.h>

namespace dionysus
{
    class Element
    {
    public:
        Element() = default;

        void draw(const dionysus::Window& window);

        void deserialize(const dionysus::Serialization& value);
        void deserialize(dionysus::Serialization&& value);

        // void update();

        void setId(std::string_view value);
        [[nodiscard]] std::string_view getId() const;

        void setPosition(const std::array<double, 2>& value);
        [[nodiscard]] const std::array<double, 2>& getPosition() const;

        [[nodiscard]] std::string_view getType() const;

    protected:
        virtual void onDeserialize(const dionysus::Serialization& value);
        // virtual void onUpdate();
        virtual void onDraw(const dionysus::Window& value);

    private:
        std::string id{};
        std ::string type{};
        std::array<double, 2> position{};
    };
}