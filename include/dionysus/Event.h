#pragma once

namespace dionysus
{
    class Event
    {
    public:
        Event() = default;
        virtual ~Event() = default;
    };
}
