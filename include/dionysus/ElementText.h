#pragma once

#include <SFML/Graphics.hpp>
#include <dionysus/Observer.h>
#include <dionysus/Element.h>
#include <string>

namespace dionysus
{
    class ElementText : public dionysus::Element, public dionysus::Observer
    {
    public:
        ElementText() = default;

        void update(const dionysus::Event* value) override;

        void setText(std::string_view value);
        [[nodiscard]] std::string_view getText() const;

        void setFont(const std::filesystem::path& value);
        [[nodiscard]] const sf::Font& getFont() const;

        void setCharacterSize(unsigned int value);
        [[nodiscard]] int getCharacterSize() const;

        void setColor(const std::array<double, 4>& value);
        [[nodiscard]] const std::array<double, 4>& getColor() const;

    protected:
        void onDraw(const dionysus::Window& window) override;
        void onDeserialize(const dionysus::Serialization& value) override;

    private:
        sf::Text sprite{};
        sf::Font font{};
        std::string text{};
        std::array<double, 4> color{};
        unsigned int characterSize{};
    };
}