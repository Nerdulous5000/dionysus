#pragma once

#include <SFML/Graphics.hpp>
#include <filesystem>

namespace dionysus
{
    class Window : public sf::RenderWindow 
    {
    public:
        Window() = default;

        /// <summary>
        /// Pass a JSON configuration to the window to set its properties.
        /// </summary>
        /// <param name="val">Path to JSON configuration file.</param>
        void setConfig(const std::filesystem::path& val);


    };
}