#pragma once

#include <dionysus/Song.h>
#include <dionysus/Subject.h>
#include <string>

namespace dionysus
{
    class SubjectSong : public Subject
    {
    public:
        void sendUpdateSong();

        void setSong(const Song& value);
        [[nodiscard]] const Song& getSong() const;

    private:
        Song song;
    };
}
