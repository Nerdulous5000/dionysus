#pragma once

#include <dionysus/Event.h>
#include <dionysus/Song.h>

namespace dionysus
{
    class EventSong : public Event
    {
    public:
        void setSong(const Song& value);
        [[nodiscard]] const Song& getSong() const;

    private:
        Song song;
    };
}
