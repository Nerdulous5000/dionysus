#pragma once

#include <dionysus/Subject.h>

namespace dionysus
{
    class SubjectVolume : public Subject
    {
    public:
        double getVolume();

    private:
        double volume;
    };
}
