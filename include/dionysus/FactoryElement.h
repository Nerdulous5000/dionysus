#pragma once

#include <concepts>
#include <dionysus/Serialization.h>
#include <dionysus/Element.h>
#include <optional>
#include <typeindex>
#include <unordered_map>

namespace dionysus
{
    template <typename T>
    concept IsElement = std::derived_from<T, Element>
    == true;
    class FactoryElement
    {
    public:
        FactoryElement() = default;

        template <typename T> void registerElement()
        {
            // this->registeredElements[typeid(T).name()] = std::make_unique<T>();
            this->elementConstructors[typeid(T).name()] = [] { return std::make_shared<T>(); };
        }

        std::shared_ptr<Element> createFromConfig(const Serialization& value);

        std::vector<std::string_view> getRegisteredElements() const;

    private:
        // type_index has no default constructor, as a lack of typeid can't produce a valid variable type. Therefore it is wrapped in an
        // std::optional, which can be checked for a valid value, before using it to instantiate an object.
        std::unordered_map<std::string, std::function<std::shared_ptr<Element>()>> elementConstructors;
    };
}