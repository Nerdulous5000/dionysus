#pragma once

#include <chrono>
#include <dionysus/Element.h>
#include <dionysus/FactoryElement.h>
#include <dionysus/Observer.h>
#include <dionysus/Subject.h>
#include <dionysus/Window.h>
#include <functional>

namespace dionysus
{
    class Kernel
    {
    public:
        Kernel();
        ~Kernel() = default;

        /// <summary>
        /// Initiate the Kernel to start an run continuously, until it is flagged to close.
        /// </summary>
        /// <returns>Kernel exit code (0 for successful exit).</returns>
        int run();

        /// <summary>
        /// Flag the kernel to close gracefully, with a successful exit code.
        /// </summary>
        void exit();

        /// <summary>
        /// Set the time in milliseconds between one cycle of the kernel
        /// running.
        /// </summary>
        /// <param name="value">Duration in milliseconds.</param>
        void setTimeStep(const std::chrono::duration<double, std::milli>& value);

        void setFactory(std::unique_ptr<dionysus::FactoryElement> value);
        dionysus::FactoryElement* getFactory() const;

        void registerAction(std::string_view actionId, std::function<void()>&& action);
        void triggerAction(std::string_view val);

        void addElement(std::shared_ptr<dionysus::Element> value);
        // void addObserver(std::shared_ptr<dionysus::Observer> value);
        void addSubject(std::shared_ptr<dionysus::Subject> value);
        [[nodiscard]] dionysus::Subject* getSubject(std::string_view value) const;

    protected:
        /// <summary>
        /// Behaviour to run once at the start of the kernel.
        /// </summary>
        /// <returns>Exit code. Return non-zero to propogate failure to
        /// application return code.</returns>
        virtual int onInit() = 0;

        /// <summary>
        /// Behaviour to run repeatedly for the lifespan on the kernel.
        /// </summary>
        /// <returns>Exit code. Return non-zero to propogate failure to
        /// application return code.</returns>
        virtual int onUpdate() = 0;

    private:
        std::vector<std::shared_ptr<dionysus::Element>> elements;
        // std::vector<std::shared_ptr<dionysus::Observer>> observers;
        std::vector<std::shared_ptr<dionysus::Subject>> subjects;
        Window window;
        std::unordered_map<std::string, std::function<void()>> actions;
        std::chrono::duration<double, std::milli> timeStep{100};
        std::unique_ptr<dionysus::FactoryElement> factory;
        std::chrono::time_point<std::chrono::steady_clock> timePrevious;
        bool shouldClose{false};
    };
}