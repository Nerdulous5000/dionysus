#pragma once

#include <boost/log/trivial.hpp>
#include <filesystem>
#include <nlohmann/json.hpp>

namespace dionysus
{
    class Serialization
    {
    public:
        Serialization() = default;

        static Serialization LoadFromFile(const std::filesystem::path& value);

        static Serialization LoadFromString(std::string_view value);

        template <typename T = std::string> T get(std::string_view value) const
        {
            T found{};
            try
            {
                // JSON paths must begin with a "/". Therefore is one does not exist, it will be added.
                std::string dataPathFormatted{value};
                constexpr auto pathRootExpected{"/"};

                if(dataPathFormatted.compare(0, 1, pathRootExpected) != 0)
                {
                    dataPathFormatted.insert(0, pathRootExpected);
                }

                const nlohmann::json_pointer<nlohmann::json> dataPath{dataPathFormatted};
                found = data[dataPath].get<T>();
            }
            catch(const std::exception& e)
            {
                BOOST_LOG_TRIVIAL(error) << "Unable to find value [" << value << "]: " << e.what();
            }
            return found;
        }

        void setFilePath(const std::filesystem::path& value);
        [[nodiscard]] const std::filesystem::path& getFilePath() const;

        void setData(const nlohmann::json& value);
        [[nodiscard]] const nlohmann::json& getData() const;

    private:
        nlohmann::json data;
        std::filesystem::path filePath;
    };
}
