#pragma once

#include <dionysus/Event.h>
#include <dionysus/Observer.h>
#include <memory>
#include <string>
#include <vector>

namespace dionysus
{
    class Subject
    {
    public:
        Subject() = default;
        virtual ~Subject() = default;

        void setId(std::string_view value);
        [[nodiscard]] std::string_view getId() const;

        void attach(std::shared_ptr<Observer> value);
        void notifyObservers(std::unique_ptr<Event> value) const;

    private:
        std::vector<std::shared_ptr<Observer>> observers{};
        std::string id;
    };
}