#pragma once

#include <dionysus/Element.h>
#include <dionysus/Observer.h>

#ifdef WIN32
#pragma warning(push, 0)
#endif
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <restc-cpp/RequestBuilder.h>
#include <restc-cpp/restc-cpp.h>
#ifdef WIN32
#pragma warning(pop)
#endif

namespace dionysus
{
    class ElementImage : public Element, public Observer
    {
    public:
        ElementImage() = default;

        void update(const dionysus::Event* value) override;

        /// <summary>
        ///
        /// </summary>
        /// <param name="value"></param>
        void setSize(const std::array<unsigned int, 2>& value);
        [[nodiscard]] const std::array<unsigned int, 2>& getSize() const;

        /// <summary>
        /// Clears the previously set size.
        /// </summary>
        /// <param name="value"></param>
        void setFilePath(const std::filesystem::path& value);
        [[nodiscard]] const std::filesystem::path& GetFilePath() const;

    protected:
        void onDraw(const dionysus::Window& window) override;
        void onDeserialize(const dionysus::Serialization& value) override;

    private:
        std::unique_ptr<restc_cpp::RestClient> client{restc_cpp::RestClient::Create()};
        sf::Sprite sprite{};
        sf::Texture texture{};
        sf::Image image{};
        std::array<unsigned int, 2> size{};
        std::array<unsigned int, 2> sizeOriginal{};
        std::filesystem::path filePath{};
    };
}