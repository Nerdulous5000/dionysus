#pragma once

#include <dionysus/Event.h>

namespace dionysus
{
    class Observer
    {
    public:
        Observer() = default;
        virtual void update(const dionysus::Event* event);
    };
}
