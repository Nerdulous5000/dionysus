#pragma once

#include <dionysus/Observer.h>
#include <iostream>

namespace dionysus
{
    class ObserverPrintSong : public Observer
    {
    public:
        ObserverPrintSong();

        void update(const Event* event) override;
    };
}
