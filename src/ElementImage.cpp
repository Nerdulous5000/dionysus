#include <boost/log/trivial.hpp>
#include <dionysus/ElementImage.h>
#include <dionysus/EventSong.h>
#include <dionysus/Utilities.h>

void dionysus::ElementImage::update(const dionysus::Event* value)
{
    auto eventSong{dynamic_cast<const dionysus::EventSong*>(value)};

    if(value != nullptr)
    {

        this->client
            ->ProcessWithPromise(
                [&](restc_cpp::Context& ctx)
                {
                    try
                    {
                        const auto urlFull{eventSong->getSong().getAlbumUrl()};
                        auto reply = restc_cpp::RequestBuilder(ctx).Get(urlFull.data()).Execute();
                        const auto responseString{reply->GetBodyAsString()};

                        std::vector<unsigned char> responseBytes(std::size(responseString));

                        std::transform(std::begin(responseString), std::end(responseString), std::begin(responseBytes),
                                       [](char c) { return static_cast<unsigned char>(c); });

                        const auto imageAlbum{cv::imdecode(responseBytes, cv::IMREAD_ANYCOLOR)};

                        cv::Mat continuousRGBA(imageAlbum.size(), CV_8UC4);
                        cv::cvtColor(imageAlbum, continuousRGBA, cv::COLOR_BGR2RGBA, 4);

                        const auto textureWidth{continuousRGBA.cols};
                        const auto textureHeight{continuousRGBA.rows};

                        this->image.create(textureWidth, textureHeight, continuousRGBA.ptr());
                        this->texture.loadFromImage(this->image);
                        this->sprite.setTexture(this->texture, true);
                        this->sizeOriginal = {static_cast<unsigned int>(textureWidth), static_cast<unsigned int>(textureHeight)};
                        this->setSize(this->size);
                    }
                    catch(const restc_cpp::RestcCppException& e)
                    {
                        BOOST_LOG_TRIVIAL(error) << e.what();
                    }
                })
            .get();
    }
}

void dionysus::ElementImage::setSize(const std::array<unsigned int, 2>& value)
{
    this->size = value;

    const auto newScaleX{static_cast<double>(value[0]) / static_cast<double>(this->sizeOriginal[0])};
    const auto newScaleY{static_cast<double>(value[1]) / static_cast<double>(this->sizeOriginal[1])};

    this->sprite.setScale(static_cast<float>(newScaleX), static_cast<float>(newScaleY));
}

const std::array<unsigned int, 2>& dionysus::ElementImage::getSize() const
{
    return this->size;
}

void dionysus::ElementImage::setFilePath(const std::filesystem::path& value)
{
    this->filePath = value;
    const auto img = cv::imread(value.string());

    cv::Mat continuousRGBA(img.size(), CV_8UC4);
    cv::cvtColor(img, continuousRGBA, cv::COLOR_RGB2RGBA, 4);

    const auto textureWidth{continuousRGBA.cols};
    const auto textureHeight{continuousRGBA.rows};

    this->image.create(textureWidth, textureHeight, continuousRGBA.ptr());
    this->texture.loadFromImage(this->image);
    this->sprite.setTexture(this->texture, true);

    this->sizeOriginal = {static_cast<unsigned int>(textureWidth), static_cast<unsigned int>(textureHeight)};
}

const std::filesystem::path& dionysus::ElementImage::GetFilePath() const
{
    return this->filePath;
}

void dionysus::ElementImage::onDraw(const dionysus::Window& value)
{
    // TODO: PLEASE I NEED TO FIND AN ALTERNATIVE TO THIS.
    const_cast<dionysus::Window*>(&value)->draw(this->sprite);
}

void dionysus::ElementImage::onDeserialize(const dionysus::Serialization& value)
{
    this->setId(value.get("id"));

    const auto posX{value.get<double>("position/x")};
    const auto posY{value.get<double>("position/y")};
    this->setPosition({posX, posY});
    this->sprite.setPosition({static_cast<float>(posX), static_cast<float>(posY)});

    const auto filePathFull{dionysus::getPathData(value.get("filePath"))};
    if(filePathFull.has_value() == true)
    {
        this->setFilePath(filePathFull.value());
    }

    this->size = {value.get<unsigned int>("size/width"), value.get<unsigned int>("size/height")};
    this->setSize(this->size);
}
