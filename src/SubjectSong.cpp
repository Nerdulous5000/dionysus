#include <dionysus/EventSong.h>
#include <dionysus/SubjectSong.h>


void dionysus::SubjectSong::sendUpdateSong()
{
    EventSong event;

    event.setSong(this->song);
    this->notifyObservers(std::make_unique<dionysus::EventSong>(event));
}

void dionysus::SubjectSong::setSong(const dionysus::Song& value)
{
    this->song = value;
    this->sendUpdateSong();
}

const dionysus::Song& dionysus::SubjectSong::getSong() const
{
    return this->song;
}
