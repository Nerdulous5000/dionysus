#include <dionysus/Element.h>


void dionysus::Element::draw(const dionysus::Window& value)
{
    this->onDraw(value);
}

void dionysus::Element::deserialize(const dionysus::Serialization& value)
{
    this->onDeserialize(value);
}

void dionysus::Element::deserialize(dionysus::Serialization&& value)
{
    this->onDeserialize(std::move(value));
}

// void dionysus::Element::update()
// {
//     this->onUpdate();
// }

void dionysus::Element::setId(std::string_view value)
{
    this->id = value;
}

std::string_view dionysus::Element::getId() const
{
    return this->id;
}

void dionysus::Element::setPosition(const std::array<double, 2>& value)
{
    this->position = value;
}

const std::array<double, 2>& dionysus::Element::getPosition() const
{
   return this->position;
}

std::string_view dionysus::Element::getType() const
{
    return this->type;
}

void dionysus::Element::onDraw(const dionysus::Window&)
{
}

void dionysus::Element::onDeserialize(const dionysus::Serialization&)
{
}

// void dionysus::Element::onUpdate()
// {
// }
