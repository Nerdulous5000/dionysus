#include <boost/log/trivial.hpp>
#include <dionysus/FactoryElement.h>

using namespace dionysus;

std::shared_ptr<Element> FactoryElement::createFromConfig(const dionysus::Serialization& value)
{
    constexpr auto pathType{"type"};

    const auto elementTypeString{value.get(pathType)};

    if(elementConstructors.contains(elementTypeString))
    {
        auto elementType{this->elementConstructors[elementTypeString]()};
        elementType->deserialize(value);
        return elementType;
    }
    else
    {
        return nullptr;
    }
}

std::vector<std::string_view> FactoryElement::getRegisteredElements() const
{
    std::vector<std::string_view> elements{};
    elements.reserve(std::size(this->elementConstructors));

    for(const auto& element : this->elementConstructors)
    {
        elements.push_back(std::get<0>(element));
    }

    return elements;
}