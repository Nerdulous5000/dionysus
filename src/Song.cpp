#include <dionysus/Song.h>

namespace
{
    constexpr auto DefaultName{"Unknown"};
    constexpr auto DefaultArtist{"Unknown"};
    constexpr auto DefaultAlbumName{"Unknown"};

    // Magenta/Black missing texture image url.
    constexpr auto DefaultAlbumUrl{"https://i.kym-cdn.com/entries/icons/original/000/028/315/cover.jpg"};
}

using namespace dionysus;

Song::Song() : name(DefaultName), artist(DefaultArtist), albumName(DefaultAlbumName), albumUrl(DefaultAlbumName)

{
}

void Song::setName(std::string_view value)
{
    this->name = value;
}

std::string_view Song::getName() const
{
    return this->name;
}

void Song::setArtist(std::string_view value)
{
    this->artist = value;
}

std::string_view Song::getArtist() const
{
    return this->artist;
}

void Song::setAlbumName(std::string_view value)
{
    this->albumName = value;
}

std::string_view Song::getAlbumName() const
{
    return this->albumName;
}

void Song::setAlbumUrl(std::string_view value)
{
    this->albumUrl = value;
}

std::string_view Song::getAlbumUrl() const
{
    return this->albumUrl;
}
