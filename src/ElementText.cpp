#include <boost/log/trivial.hpp>
#include <dionysus/Serialization.h>
#include <dionysus/ElementText.h>
#include <dionysus/EventSong.h>
#include <dionysus/Utilities.h>

namespace
{
    sf::Color ToSFColor(const std::array<double, 4>& value)
    {
        constexpr auto ColorDoubleToUint{255.0};
        return {static_cast<sf::Uint8>(value[0] * ColorDoubleToUint), static_cast<sf::Uint8>(value[1] * ColorDoubleToUint),
                static_cast<sf::Uint8>(value[2] * ColorDoubleToUint), static_cast<sf::Uint8>(value[3] * ColorDoubleToUint)};
    }
}

void dionysus::ElementText::update(const dionysus::Event* value)
{
    auto eventSong{dynamic_cast<const dionysus::EventSong*>(value)};

    if(value != nullptr)
    {
        this->sprite.setString(eventSong->getSong().getName().data());
        this->text = eventSong->getSong().getName().data();
    }
}

void dionysus::ElementText::setText(std::string_view value)
{
    this->text = value;
    this->sprite.setString(this->text);
}

std::string_view dionysus::ElementText::getText() const
{
    return this->text;
}

void dionysus::ElementText::setFont(const std::filesystem::path& value)
{
    const auto pathFont{dionysus::getPathData(value)};
    if(this->font.loadFromFile(pathFont.value().string()) == true)
    {
        this->sprite.setFont(this->font);
    }
    else
    {
        BOOST_LOG_TRIVIAL(error) << "Unable to load font: " << value;
    }
}

const sf::Font& dionysus::ElementText::getFont() const
{
    return this->font;
}

void dionysus::ElementText::setCharacterSize(unsigned int value)
{
    this->characterSize = value;
    this->sprite.setCharacterSize(value);
}

int dionysus::ElementText::getCharacterSize() const
{
    return this->characterSize;
}

void dionysus::ElementText::setColor(const std::array<double, 4>& value)
{
    this->color = value;
    this->sprite.setFillColor(ToSFColor(value));
}

const std::array<double, 4>& dionysus::ElementText::getColor() const
{
    return this->color;
}

void dionysus::ElementText::onDraw(const dionysus::Window& value)
{
    // TODO: PLEASE I NEED TO FIND AN ALTERNATIVE TO THIS.
    const_cast<dionysus::Window*>(&value)->draw(this->sprite);
}

void dionysus::ElementText::onDeserialize(const dionysus::Serialization& value)
{
    this->setId(value.get("id"));

    const auto posX{value.get<double>("position/x")};
    const auto posY{value.get<double>("position/y")};
    this->setPosition({posX, posY});
    this->sprite.setPosition(static_cast<float>(posX), static_cast<float>(posY));

    this->setText(value.get("text"));
    this->setFont(value.get("font"));
    this->setColor(value.get<std::array<double, 4>>("color"));
    this->setCharacterSize(value.get<unsigned int>("characterSize"));
}
