#include <dionysus/Subject.h>

using namespace dionysus;

void dionysus::Subject::setId(std::string_view value)
{
    this->id = value;
}

std::string_view dionysus::Subject::getId() const
{
    return this->id;
}

void ::Subject::attach(std::shared_ptr<Observer> value)
{
    this->observers.push_back(value);
}

void Subject::notifyObservers(std::unique_ptr<Event> value) const
{
    for(auto& observer : this->observers)
    {
        observer->update(value.get());
    }
}
