#include <dionysus/EventSong.h>

using namespace dionysus;

void dionysus::EventSong::setSong(const Song& value)
{
    this->song = value;
}

const Song& dionysus::EventSong::getSong() const
{
    return this->song;
}
