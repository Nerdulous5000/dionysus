#include <dionysus/Serialization.h>
#include <dionysus/Utilities.h>
#include <fstream>
#include <nlohmann/json.hpp>

dionysus::Serialization dionysus::Serialization::LoadFromFile(const std::filesystem::path& value)
{
    dionysus::Serialization config;

    config.setFilePath(value);
    const auto filePathFull{dionysus::getPathData(value)};
    nlohmann::json data{};

    if(filePathFull.has_value() == true)
    {
        std::ifstream stream(filePathFull.value());

        try
        {
            data = nlohmann::json::parse(stream);

        }
        catch(const std::exception& e)
        {
            BOOST_LOG_TRIVIAL(error) << "Unable to load file [" << value << "]: " << e.what();
        }
    }

    config.setData(data);

    return config;
}

dionysus::Serialization dionysus::Serialization::LoadFromString(std::string_view value)
{
    dionysus::Serialization config;

    nlohmann::json data{};

    try
    {
        data = nlohmann::json::parse(value);

    }
    catch(const std::exception& e)
    {
        BOOST_LOG_TRIVIAL(error) << "Unable to load file [" << value << "]: " << e.what();
    }

    config.setData(data);

    return config;
}

void dionysus::Serialization::setFilePath(const std::filesystem::path& value)
{
    this->filePath = value;
}

const std::filesystem::path& dionysus::Serialization::getFilePath() const
{
    return this->filePath;
}

void dionysus::Serialization::setData(const nlohmann::json& value)
{
    this->data = value;
}

const nlohmann::json& dionysus::Serialization::getData() const
{
    return this->data;
}
