#include <dionysus/ElementImage.h>
#include <dionysus/ElementText.h>
#include <dionysus/Kernel.h>
#include <dionysus/Setup.h>
#include <dionysus/SubjectSong.h>

void dionysus::Setup(dionysus::Kernel& value)
{
    auto factory{value.getFactory()};
    dionysus::SubjectSong subjectSong{};
    subjectSong.setId("subjectSong");

    factory->registerElement<dionysus::ElementText>();
    const auto configTextTitle{dionysus::Serialization::LoadFromFile("config/elementTitle.config")};
    auto elementTitle{factory->createFromConfig(configTextTitle)};
    subjectSong.attach(dynamic_pointer_cast<dionysus::Observer>(elementTitle));
    value.addElement(std::move(elementTitle));

    factory->registerElement<dionysus::ElementImage>();
    const auto configTextImage{dionysus::Serialization::LoadFromFile("config/elementAlbumCover.config")};
    auto elementImage{factory->createFromConfig(configTextImage)};
    subjectSong.attach(dynamic_pointer_cast<dionysus::Observer>(elementImage));
    value.addElement(std::move(elementImage));

    value.addSubject(std::make_shared<dionysus::SubjectSong>(subjectSong));
}
