#include <dionysus/Kernel.h>
#include <dionysus/Setup.h>

using namespace dionysus;

Kernel::Kernel() : timePrevious{std::chrono::steady_clock::now()}, factory{std::make_unique<dionysus::FactoryElement>()}
{
    dionysus::Setup(*this);

    this->window.setConfig("config/window.config");
}

int Kernel::run()
{
    auto exitCode{0};

    exitCode = this->onInit();

    while(this->shouldClose == false && exitCode == 0 && this->window.isOpen())
    {
        sf::Event event;
        while(this->window.pollEvent(event))
        {
            if(event.type == sf::Event::Closed)
            {

                this->window.close();
            }
        }

        const auto timeNow{std::chrono::steady_clock::now()};

        // Limit time between updates to logic/state.
        if(timeNow - this->timePrevious > this->timeStep)
        {
            this->timePrevious = timeNow;

            exitCode = this->onUpdate();
        }

        this->window.setActive();
        this->window.clear();

        for(auto& element : this->elements)
        {
            element->draw(this->window);
        }

        this->window.display();
    }

    return exitCode;
}

void Kernel::exit()
{
    this->shouldClose = true;
}

void dionysus::Kernel::setTimeStep(const std::chrono::duration<double, std::milli>& value)
{
    this->timeStep = value;
}

void dionysus::Kernel::setFactory(std::unique_ptr<dionysus::FactoryElement> value)
{
    this->factory = std::move(value);
}

dionysus::FactoryElement* dionysus::Kernel::getFactory() const
{
    return this->factory.get();
}

void Kernel::registerAction(std::string_view id, std::function<void()>&& action)
{
    this->actions[id.data()] = std::move(action);
}

void Kernel::triggerAction(std::string_view val)
{
    const auto& action{this->actions[val.data()]};
    action();
}

void dionysus::Kernel::addElement(std::shared_ptr<dionysus::Element> value)
{
    this->elements.push_back(std::move(value));
}

// void dionysus::Kernel::addObserver(std::shared_ptr<dionysus::Observer> value)
// {
//     this->observers.push_back(std::move(value));
// }

void dionysus::Kernel::addSubject(std::shared_ptr<dionysus::Subject> value)
{
    this->subjects.push_back(std::move(value));
}

dionysus::Subject* dionysus::Kernel::getSubject(std::string_view value) const
{
    auto found{std::find_if(std::begin(this->subjects), std::end(this->subjects),
                            [&value](const auto& subject) { return subject->getId() == value; })};
    if(found != std::end(this->subjects))
    {
        return found->get();
    }
    else
    {
        return nullptr;
    }
}
