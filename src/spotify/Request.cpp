#include <boost/log/trivial.hpp>
#include <dionysus/spotify/Request.h>

namespace
{

    std::string UrlBase{"https://api.spotify.com/v1/me/"};
}

void dionysus::spotify::Request::send(restc_cpp::RestClient* value)
{
    // Tutorial found here:
    // https://github.com/jgaa/restc-cpp/blob/master/doc/GettingStarted.md
    value
        ->ProcessWithPromise(
            [&](restc_cpp::Context& ctx)
            {
                try
                {
                    const auto urlFull{UrlBase + this->getEndpoint().data()};
                    auto reply = restc_cpp::RequestBuilder(ctx)
                                     .Get(urlFull)
                                     .AddHeader("Authorization", "Bearer " + token)
                                     .DisableCompression()
                                     .Execute();
                    body = dionysus::Serialization::LoadFromString(reply->GetBodyAsString());
                }
                catch(const restc_cpp::RestcCppException& e)
                {
                    BOOST_LOG_TRIVIAL(error) << e.what();
                }
            })
        .get();
}

const std::optional<dionysus::Serialization>& dionysus::spotify::Request::getBody() const
{
    return this->body;
}

void dionysus::spotify::Request::setEndpoint(std::string_view value)
{
    this->endpoint = value;
}

std::string_view dionysus::spotify::Request::getEndpoint() const
{
    return this->endpoint;
}

void dionysus::spotify::Request::setToken(std::string_view value)
{
    this->token = value;
}

std::string_view dionysus::spotify::Request::getToken() const
{
    return this->token;
}

void dionysus::spotify::Request::setType(dionysus::spotify::RequestType value)
{
    this->type = value;
}

dionysus::spotify::RequestType dionysus::spotify::Request::getType() const
{
    return this->type;
}
