#include <boost/log/trivial.hpp>
#include <dionysus/Serialization.h>
#include <dionysus/SubjectSong.h>
#include <dionysus/Utilities.h>
#include <dionysus/spotify/KernelSpotify.h>
#include <dionysus/spotify/Request.h>
#include <nlohmann/json.hpp>

namespace
{
    // File relative to data directory.
    constexpr auto PathToken{"token.json"};

    constexpr auto PathId{"item/id"};
    constexpr auto PathSongName{"item/name"};
    constexpr auto PathArtist{"item/artists/0/name"};
    constexpr auto PathAlbumName{"item/album/name"};
    constexpr auto PathAlbumUrl{"item/album/images/0/url"};
}

int dionysus::spotify::KernelSpotify::onInit()
{
    return 0;
}

int dionysus::spotify::KernelSpotify::onUpdate()
{
    const auto token{this->getToken()};

    dionysus::spotify::Request request{};
    request.setEndpoint("player");
    request.setToken(token);
    request.send(this->client.get());
    const auto body{request.getBody()};
    if(body.has_value() == true)
    {
        const auto songId{body.value().get(PathId)};

        if(songId != this->songIdPrevious)
        {

            auto subject{this->getSubject("subjectSong")};
            auto subjectSong{dynamic_cast<dionysus::SubjectSong*>(subject)};

            if(subjectSong != nullptr)
            {
                dionysus::Song song{};
                
                song.setName(body.value().get(PathSongName));
                song.setAlbumUrl(body.value().get(PathAlbumUrl));
                song.setAlbumName(body.value().get(PathAlbumName));
                song.setArtist(body.value().get(PathArtist));

                subjectSong->setSong(song);
            }

            this->songIdPrevious = songId;
        }
    }

    return 0;
}

std::string dionysus::spotify::KernelSpotify::getToken()
{
    const auto config{dionysus::Serialization::LoadFromFile(PathToken)};
    const auto token{config.get("access_token")};

    return token;
}
