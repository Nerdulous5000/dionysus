#include <dionysus/Utilities.h>
#include <dionysus/Window.h>
#include <dionysus/Serialization.h>

namespace
{
    constexpr auto NameWidth{"width"};
    constexpr auto NameHeight{"height"};
    constexpr auto NameFramerate{"framerate"};
    constexpr auto NameFullscreen{"fullscreen"};
    constexpr auto NameTitle{"title"};
}

void dionysus::Window::setConfig(const std::filesystem::path& value)
{
    sf::RenderWindow::close();
    const auto config{dionysus::Serialization::LoadFromFile(value)};

    // Fill data fields.
    const auto width{config.get<unsigned int>(NameWidth)};
    const auto height{config.get<unsigned int>(NameHeight)};
    const auto title{config.get<std::string>(NameTitle)};
    const auto fullscreen{config.get<bool>(NameFullscreen)};

    const auto style{fullscreen == true ? sf::Style::Fullscreen : sf::Style::Default};
    sf::Window::create(sf::VideoMode(width, height), title, style);
    sf::Window::setSize({width, height});

    const auto framerate{config.get<unsigned int>(NameFramerate)};

    sf::RenderWindow::setFramerateLimit(framerate);
}

