#include <cmake/CMakeConfig.hxx>
#include <dionysus/Utilities.h>
#include <iostream>
#include <fstream>
#include <boost/log/trivial.hpp>

namespace
{
    constexpr auto PathData{"data"};
}

std::optional<std::filesystem::path>
dionysus::getPathData(const std::filesystem::path& value)
{
    const auto file{cmake::PathProject / PathData / value};
    if(std::filesystem::exists(file) == true)
    {
        return file;
    }
    else
    {
        BOOST_LOG_TRIVIAL(error) << "Unable to find file: " << file << "\n";
        return std::nullopt;
    }
}

std::optional<std::filesystem::path>
dionysus::getPathProject(std::string_view value)
{
    const auto file{cmake::PathProject / value};
    if(std::filesystem::exists(file) == true)
    {
        return file;
    }
    else
    {
        std::cout << "Unable to find file: " << file << "\n";
        return std::nullopt;
    }
}

// nlohmann::json dionysus::parseJsonFile(const std::filesystem::path& value)
// {
//     const auto pathConfigFull{dionysus::getPathData(value.string())};
//     nlohmann::json json{};
// 
//     if(pathConfigFull.has_value() == true)
//     {
//         // Deserialize token file and get token value.
//         std::ifstream fileString(pathConfigFull.value().string());
//         fileString >> json;
//     }
// 
//     return json;
// }

// nlohmann::json dionysus::parseJsonString(std::string_view value)
// {
//     nlohmann::json json{};
// 
//     try
//     {
//         json = nlohmann::json::parse(value);
//     }
//     catch(const std::exception& e)
//     {
//         BOOST_LOG_TRIVIAL(error) << "Unable to load json: " << e.what();
//     }
// 
//     return json;
// }