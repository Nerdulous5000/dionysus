#include <dionysus/EventSong.h>
#include <dionysus/ObserverPrintSong.h>

using namespace dionysus;

ObserverPrintSong::ObserverPrintSong()
{
}

void ObserverPrintSong::update(const dionysus::Event* event)
{
    auto eventSong{dynamic_cast<const dionysus::EventSong*>(event)};

    if(event != nullptr)
    {
        std::cout << "[Song Update]: " << eventSong->getSong().getName() << "\n";
    }
}
